# WHILL Noetic
ROS1 Noetic package for controlling the autonomous wheelchair WHILL.

## What's New


## System Configurations
### WHILL
[WHILL Model CR](https://whill.inc/jp/model-cr)<br>
[Officeal Site](https://whill.inc/jp/model-cr)<br>

The research and development model WHILL Model CR can control the main unit by sending signals from external devices (communication method: RS232C).

It can also get information on the main unit (speed, acceleration/deceleration values, encoder information, acceleration sensor values, controller input information, battery information, etc.) and can be used as a research and business platform in the fields of automatic driving, collision avoidance, mobility support, and autonomous driving.

https://whill.inc/jp/wp-content/themes/whill-jp/pdf/whill_model_cr_reaf.pdf?v=0824

### Control PC to be mounted on WHILL
A PC meeting the following specifications is required.

- Ubuntu20.04
- ROS1 Noetic
- NVIDIA

### LiDAR
#### 2D
Top-URG（Hokuyo UTM-30LX）

#### 3D
Velodyne VLP-16

### RGB Camera

### Game Controller


## Setup
### Ubuntu20.04
### ROS1 Noetic
```
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
catkin_init_workspace
cd ~/catkin_ws
catkin_make
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

### Install necessary drivers
```
sudo apt-get update
sudo apt-get install -y ros-noetic-joy ros-noetic-teleop-twist-joy ros-noetic-urg-node ros-noetic-serial 
sudo apt-get install -y ros-noetic-gmapping libbullet-dev libsdl-image1.2-dev libsdl-dev ros-noetic-navigation 
ros-noetic-geometry2
sudo apt install -y ros-noetic-usb-cam ros-noetic-image-view
```

### Installation and setup of Velodyne drivers
```
sudo apt-get install ros-noetic-velodyne 
cd ~/catkin_ws/src
git clone https://github.com/ros-drivers/velodyne.git
cd ~/catkin_ws
rosdep install --from-paths src --ignore-src --rosdistro noetic -y 
catkin_make
source ~/.bashrc
```

### Installation of ROS package for WHILL

```
cd ~/catkin_ws/src
git clone https://github.com/WHILL/ros_whill.git
git clone https://gitlab.com/tidbots/whill/whill_noetic.git
cd ~/catkin_ws
catkin_make
source ~/.bashrc
```

If there are no errors, move the WHILL as soon as possible.
Move to a wide open space with no obstacles or hazards around.

## Controlled from a terminal
Check the serial port to which the control PC and WHILL are connected.

```
ls -lah /dev/serial/by-id/
total 0
drwxr-xr-x 2 root root 80 11月  4 07:44 .
drwxr-xr-x 4 root root 80 11月  4 07:44 ..
lrwxrwxrwx 1 root root 13 11月  4 07:44 usb-FTDI_USB_HS_SERIAL_CONVERTER_FTDF0EU1-if00-port0 -> ../../ttyUSB0
lrwxrwxrwx 1 root root 13 11月  4 07:44 usb-Hokuyo_Data_Flex_for_USB_URG-Series_USB_Driver-if00 -> ../../ttyACM0

```

Now that we know that the serial port is /dev/ttyUSB0, we set this to the environment variable TTY_WHILL. To simplify the process of moving, write the following in your .bashrc.
Here, /dev/ttyUSB0 should be changed according to your environment.
```
echo 'export TTY_WHILL=/dev/ttyUSB0' >> ~/.bashrc
source ~/.bashrc
```

Usually, the serial port authority is not enough, so after granting the authority, launch ros_whill.launch.
```
sudo chmod 666 /dev/ttyUSB0
roslaunch ros_whill ros_whill.launch
```
If there are no errors, check the list of topics with the rostopic command.
Start another terminal and execute the following command.

```
rostopic list

/rosout
/rosout_agg
/tf
/tf_static
/whill/controller/cmd_vel
/whill/controller/joy
/whill/odom
/whill/states/batteryState
/whill/states/jointState
/whill/states/joy
```

/whill/controller/cmd_velに直接データを送ることでWHILLを動かしてみましょう。
もう一度、周囲に障害物がないことを確認してから下記のコマンドを実行して下さい。
```
rostopic pub -1 /whill/controller/cmd_vel geometry_msgs/Twist -- '[1.0, 0.0, 0.0]' '[0.0, 0.0, 1.5]'
```
WHILLが少しだけ動けば、動作確認は成功です。

### ゲームパッドで動かす
エレコム（JC-U4113SBK）<br>
<img src="https://scrapbox.io/files/6364530f593371001dfa2e9e.png" width="256">
<img src="https://scrapbox.io/files/636454133525b4001d89b5c8.png" width="256">

ゲームパッドが制御用PCのUSBポートに接続されていることを確認して、下記のコマンドを実行して下さい。<br>

```
roslaunch whill_teleop teleop.launch
```

ゲームパッドのLBボタン（左人差し指）を押しながら左スティックを回すとWHILLが移動します．

<img src="http://www.wanpug.com/illust3464_thumb.gif" width="128">


### Top-Urgの動作確認
#### ドライバーのインストール
<img src="http://www.wanpug.com/illust3464_thumb.gif" width="128">

#### シリアルポートの確認
Top-Urgを使うために、制御用PCと接続されているシリアルポートを確認します。
```
ls -lah /dev/serial/by-id/
total 0
drwxr-xr-x 2 root root 80 11月  4 07:44 .
drwxr-xr-x 4 root root 80 11月  4 07:44 ..
lrwxrwxrwx 1 root root 13 11月  4 07:44 usb-FTDI_USB_HS_SERIAL_CONVERTER_FTDF0EU1-if00-port0 -> ../../ttyUSB0
lrwxrwxrwx 1 root root 13 11月  4 07:44 usb-Hokuyo_Data_Flex_for_USB_URG-Series_USB_Driver-if00 -> ../../ttyACM0
```
ここでは、シリアルポートが /dev/ttyACM0 に設定されています。
通常はシリアルポートの権限が足りないので、権限を付与します。
```
sudo chmod 666 /dev/ttyACM0
```
#### ROSノードの起動
roscoreを起動した後に、別の端末でurg_nodeを起動します。
```
roscore
```
別の端末で
```
rosrun urg_node urg_node _serial_port:="/dev/ttyACM0"
```
エラー（端末に赤い文字で表示される）が無ければ、urg_nodeの起動は成功です。

#### データの表示
ROSに標準で用意されている可視化ツールの rviz でTop-URGが取得したセンサーデータを確認してみましょう。
端末を起動し、rvizを実行します。
```
rviz
```
rvizのGUIが立ち上がったのを確認したら、下記のようにFixed Frameをデフォルトのmapからlaserに変えて下さい。<br>
<img src="https://scrapbox.io/files/6364508bf337b3001d2139a7.png]" width="512">

左下の「Add」ボタンを押し、「By topic」タブから LaserScanをダブルクリックします<br>
<img src="https://scrapbox.io/files/6364508ff90da60023d6566e.png" width="512">


下記のようにTop-URGからの距離情報が取得でれば成功です。 色の付いている各点が取得した位置情報で、 各点の色が強度情報(紫が強く赤が弱い)を表しています。<br>
<img src="https://scrapbox.io/files/63645096d2c5d2001d29f7b5.png" width="512">


# シリアルポートの権限のこと
シリアルポートの権限は制御用PCを再起動するごとに元に戻ってしまいます。そこで、下記のように設定を変更しておくと便利です。

/lib/udev/rules.d/50-udev-default.rules を管理者権限で下記のように編集します。
```
sudo vi /lib/udev/rules.d/50-udev-default.rules
```
下記の通り書き換えます．
```
（修正前）　KERNEL=="tty[A-Z]*[0-9]|pppox[0-9]*|ircomm[0-9]*|noz[0-9]*|rfcomm[0-9]*", GROUP="dialout"
（修正後）　KERNEL=="tty[A-Z]*[0-9]|pppox[0-9]*|ircomm[0-9]*|noz[0-9]*|rfcomm[0-9]*", GROUP="dialout", MODE="0777"
```
さらに下記のように、ユーザ権限でアクセスできるように変更します。
```
sudo usermod -a -G dialout $USER
```
このようにしておけばUSBシリアルデバイスを使う際には自動的に権限が付与されるようになります．

### Velodyneの動作確認



### USBカメラ画像の確認
#### ROSノードの起動
roscoreを起動した後に、別の端末でusb_cam_nodeを起動します。
```
roscore
```
別の端末で
```
rosrun usb_cam usb_cam_node 
```
エラー（端末に赤い文字で表示される）が無ければ、usb_cam_nodeの起動は成功です。

#### カメラ画像の表示
ROSに標準で用意されている可視化ツールの rqt_image_view　でWebカメラが取得した画像を確認してみましょう。
端末を起動し、rqt_image_viewを実行します。
```
rqt_image_view
```
rqt_image_viewのGUIが立ち上がったのを確認したら、下記のように表示するトピックを/usb_cam/image_raw に変更して下さい．。Webカメラからの画像が表示されるはずです。<br>
<img src="https://scrapbox.io/files/636468c3327177001df06013.png]" width="512">


# 2DNavigation Stackを使った自律移動
## 地図の作成
### Velodyneの場合
ターミナルを開き、下記のコマンドを実行します。
```
roslaunch whill_bringup velodyne.launch 
```
別のターミナルを開き、下記のコマンドを実行します。
```
roslaunch whill_navigation gmapping_velodyne.launch 
```

あるいは、以下のコマンド一発で
```
roslaunch will_apps make_map_velodyne.launch
```

### Top-URGの場合
```
roslaunch whii_bringup urg.launch
```
別のターミナルを開き、下記のコマンドを実行します。
```
roslaunch whill_navigation gmapping_urg.launch 
```

起動した位置が原点になるので、ジョイスティックで移動させると地図が作成されます。


<img src="https://scrapbox.io/files/637866a266ebaf001f0da43b.jpg" width="256">
<img src="https://scrapbox.io/files/637866f7392cec001d16b34d.jpg" width="256">


## 地図の保存
作成したmapを下記のように保存します。
mapの保存にはmap_serverパッケージのmap_saverノードを使用します。
次のコマンドを実行することで、ホームフォルダに地図の画像ファイル(.pgm)とデータファイル(.yaml)が保存されます。
ファイル名は適宜設定してください。
```
rosrun map_server map_saver -f ファイル名
rosrun map_server map_saver map:=/map -f my_map

roslaunch whill_apps make_map.lauhch
roslaunch whill_apps save_map.launch map:=マップ名
```

<img src="http://www.wanpug.com/illust3464_thumb.gif" width="128">


## 作成した地図を使った２次元ナビゲーション
作成した地図は ~/catkin_ws/src/whill-project/whii_navigation/maps に保存して下さい。

### Velodyneの場合
ターミナルを開き、下記のコマンドを実行します。
```
roslaunch whill_bringup velodyne.launch 
```
別のターミナルを開き、下記のコマンドを実行します。
```
roslaunch whill_navigation auto_velodyne.launch 
```

あるいは、以下のコマンド一発で
```
roslaunch will_apps auto_navigation_velodyne.launch
```


### Top-URGの場合
```
roslaunch whii_bringup urg.launch
```
別のターミナルを開き、下記のコマンドを実行します。
```
roslaunch whill_navigation auto_urg.launch 
```


RVizの「2D Nav Goal」で2次元ナビゲーションの目標位置を指定すると、指定した座標に移動します。

緊急停止ボタンをすぐに押せる状態で試して下さい。

<img src="https://zet-art.net/wp-content/uploads/2019/03/caution-01.png" width="128"><br>
<img src="http://www.wanpug.com/illust3464_thumb.gif" width="128">

# 参考にした情報
[WHILL（ウィル）株式会社](https://whill.inc/jp/)<br>
[WHILL 公式Github](https://github.com/WHILL)<br>
[ROS2対応](https://github.com/orgs/whill-labs/repositories)<br>
[Model CRを動かせるライブラリたち](https://note.com/katsushun89/n/n6287998d48a6)<br>

[WHILL(ROS対応)をUbuntuPCとraspi経由でDS4から動かす](https://qiita.com/noby_aoao/items/026068c944067d4a4288)<br>
[ROS Navigation Stack](https://github.com/ros-planning/navigation.git)<br>

ナビゲーションはこちらを参考
WHILL_Auto
https://qiita.com/noby_aoao/private/9d1734e80ae7f31f965d
https://github.com/nobunoby/WHILL_Auto

https://vstoneofficial.github.io/lightrover_webdoc/software/gmapping/
SLAMを使ってみる
https://qiita.com/motoms/items/4c45f75911e210088ea1

gmapping
git clone https://github.com/ros-perception/slam_gmapping
git clone https://github.com/ros-perception/openslam_gmapping.git

TF(Transform Tree)
<img src="https://scrapbox.io/files/6365cab6c05ac80023a90a88.png]" width="512"><br>

<img src="https://scrapbox.io/files/6365cb42563be2001d5ac7a4.png]" width="256">
<img src="https://scrapbox.io/files/6365cb850bfeb80021507391.png]" width="128">


[navigation ROS公式](http://wiki.ros.org/ja/navigation)<br>
[ROS Navigationスタック　ソフトウェア設計仕様（産総研）](https://robo-marc.github.io/navigation_documents/index.html)<br>
[ROS Navigation Stack について 1](https://daily-tech.hatenablog.com/entry/2017/02/10/074925)<br>
[ROS- Navigation Stack - HackMD](https://hackmd.io/@Thk133gjTHaPOdqIhuHQxg/BJzdUzkqr)<br>
[Navigation Stack を理解する - 1. 導入](https://qiita.com/MoriKen/items/0b75ab291ab0d95c37c2)<br>
[SLAMして自己位置推定して自律移動する](https://qiita.com/noby_aoao/items/9d1734e80ae7f31f965d)<br>
[移動ロボットのためにROSパッケージの紹介と実機への導入方法（上田隆一：千葉工大）](https://www.jstage.jst.go.jp/article/sicejl/57/10/57_715/_pdf/-char/ja)<br>
[How to Set Up the ROS Navigation Stack on a Robot](https://automaticaddison.com/how-to-set-up-the-ros-navigation-stack-on-a-robot/)<br>
[SLAMをしよう](https://vstoneofficial.github.io/lightrover_webdoc/software_ubuntu/gmapping/)

# Authors
Hiroyuki Okada

# Contact
admin@okadanet.org

# LICENSE
This software is released under the GNU GENERAL PUBLIC LICENSE Version 3, see LICENSE.txt.

